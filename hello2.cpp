///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04b - Hello World II
///
/// This is an Object Oriented C++ Hello World program
///
/// @file hello2.cpp
/// @version 1.0
///
/// @author Colby Kagamida <colbykag@hawaii.edu>
/// @date 14 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>  //no namespace std, using std:: instead

int main() {

   std::cout << "Hello World" << std::endl;  //using endl instead of \n

   return 0;
}

