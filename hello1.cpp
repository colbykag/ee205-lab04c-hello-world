///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04c - Hello World II
///
/// This is an Object Oriented C++ Hello World program
///
/// @file hello1.cpp
/// @version 1.0
///
/// @author Colby Kagamida <colbykag@hawaii.edu>
/// @date 14 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

using namespace std;//using std and no std::

int main() {

   cout << "Hello World" << endl;//using endl instead of '\n'

   return 0;
}

